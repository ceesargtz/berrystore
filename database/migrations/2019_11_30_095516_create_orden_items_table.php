<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('precio',8,2);
            $table->integer('cantidad')->unsigned();
            $table->unsignedBigInteger('id_producto');
            $table->foreign('id_producto')
                  ->references('id')->on('productos')
                  ->onDelete('cascade');
            $table->unsignedBigInteger('id_orden');
            $table->foreign('id_orden')
                  ->references('id')->on('ordenes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_items');
    }
}
