<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',255);
            $table->string('url');
            $table->text('descripcion');
            $table->string('descripcion_corta',300);
            $table->decimal('precio',8,2);
            $table->string('imagen',300);
            $table->boolean('visible');
            $table->unsignedBigInteger('id_categoria');
            $table->foreign('id_categoria')
                  ->references('id')->on('categorias')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
