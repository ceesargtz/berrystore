<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class tablaProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data=array([
        'nombre'=>'Laptop Lenovo V130-14IGM',
        'url' =>'Laptop-lenovo-v130',
        'descripcion' => 'Características
                          Familia de procesador: Intel Celeron
                          Diagonal de la pantalla: 14pulg.
                          Memoria interna: 4 GB
                          Tipo de memoria interna: DDR4-SDRAM
                          Capacidad total de almacenaje: 500 GB
                          Sistema operativo instalado: Windows 10 Home',
        'descripcion_corta'=> '14 pulgadas HD, Intel Celeron N4000 1.10GHz, 4GB, 500GB, Windows 10 Home 64-bit, Gris',
        'precio'=> 5420.00,
        'imagen'=> 'https://www.cyberpuerta.mx/img/product/M/CP-LENOVO-81HM009SLM-1.jpg',
        'visible'=> true,
        'id_categoria'=> 1,
        'created_at'=> new DateTime,
        'updated_at'=> new DateTime
      ],
      [
        'nombre'=>'Laptop Gamer Lenovo IdeaPad 330S-15IKB ',
        'url' =>'ideaPad-330s',
        'descripcion' => 'Características
                          Familia de procesador: Intel® Core™ i5-8xxx
                          Diagonal de la pantalla: 15.6pulg.
                          Memoria interna: 8 GB
                          Tipo de memoria interna: DDR4-SDRAM
                          Capacidad total de almacenaje: 1016 GB
                          Sistema operativo instalado: Windows 10 Home
                          ',
        'descripcion_corta'=> '15.6 pulgadas HD, Intel Core i5-8250U 1.60GHz, 8GB, 16GB Optane, 1TB, NVIDIA GeForce GTX 1050, Windows 10 Home 64-bit, Plata',
        'precio'=> 9000.00,
        'imagen'=> 'https://www.cyberpuerta.mx/img/product/M/CP-LENOVO-81GC0033LM-1.jpg',
        'visible'=> true,
        'id_categoria'=> 1,
        'created_at'=> new DateTime,
        'updated_at'=> new DateTime
      ],
      [
        'nombre'=>'Laptop Lenovo IdeaPad 330',
        'url' =>'ideapad-330',
        'descripcion' => 'Características
                          Familia de procesador: AMD A
                          Diagonal de la pantalla: 14pulg.
                          Memoria interna: 8 GB
                          Tipo de memoria interna: DDR4-SDRAM
                          Capacidad total de almacenaje: 1000 GB
                          Sistema operativo instalado: Windows 10 Home',
        'descripcion_corta'=> '14 pulgadas HD, AMD A6-9225 2.60GHz, 8GB, 1TB, Windows 10 Home 64-bit, Azul',
        'precio'=> 7239.00,
        'imagen'=> 'https://www.cyberpuerta.mx/img/product/M/CP-LENOVO-81D5001ELM-1.jpg',
        'visible'=> true,
        'id_categoria'=> 1,
        'created_at'=> new DateTime,
        'updated_at'=> new DateTime
      ],
      [
        'nombre'=>'Laptop HP 15-DA0001LA ',
        'url' =>'hp-15da0001la',
        'descripcion' => 'Características
                          Familia de procesador: Intel Celeron
                          Diagonal de la pantalla: 15.6pulg.
                          Memoria interna: 4 GB
                          Tipo de memoria interna: DDR4-SDRAM
                          Capacidad total de almacenaje: 500 GB
                          Sistema operativo instalado: Windows 10 Home',
        'descripcion_corta'=> '15.6 pulgadas HD, Intel Celeron N4000 2.60GHz, 4GB, 500GB, Windows 10 Home 64-bit, Gris/Plata',
        'precio'=> 5469.00,
        'imagen'=> 'https://www.cyberpuerta.mx/img/product/M/CP-HP-3PX26LA-1.jpg',
        'visible'=> true,
        'id_categoria'=> 1,
        'created_at'=> new DateTime,
        'updated_at'=> new DateTime
      ],
      [
        'nombre'=>'Laptop Lenovo Ideapad S145-14AST',
        'url' =>'ideapad-s145',
        'descripcion' => 'Características
                          Familia de procesador: AMD A
                          Diagonal de la pantalla: 14pulg.
                          Memoria interna: 4 GB
                          Capacidad total de almacenaje: 500 GB',
        'descripcion_corta'=> '14 pulgadas HD, AMD A4-9125 2.30GHz, 4GB, 500GB, Windows 10 Home 64-bit, Plata',
        'precio'=> 4829.00,
        'imagen'=> 'https://www.cyberpuerta.mx/img/product/M/CP-LENOVO-81ST0000LM-1.jpg',
        'visible'=> true,
        'id_categoria'=> 1,
        'created_at'=> new DateTime,
        'updated_at'=> new DateTime
      ]);
    DB::table('productos')->insert($data);
    }
}
