<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class tablaCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categorias')->insert([
        'nombre'=>'Laptops',
        'url' =>'laptops',
        'descripcion' => 'Laptops de diferentes marcas'
      ]);
    }
}
