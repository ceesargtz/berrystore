<?php

use Illuminate\Database\Seeder;
use App\User;

class tablaUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
          [
            'nombres' => 'administrador',
            'apellidos' => 'berryStore',
            'correo' => 'administrador@berrystore.com',
            'usuario' => 'administrador',
            'password' => \Hash::make('admin1234'),
            'rol' => 'administrador',
            'activo' => 1,
            'direccion' => 'Rep. de Argentina, Saltillo, Coahuila',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
            ],
            [
              'nombres' => 'César Gerardo',
              'apellidos' => 'Gutiérrez López',
              'correo' => 'ceesar_gtz@hotmail.com',
              'usuario' => 'cesargtz',
              'password' => \Hash::make('usuario1234'),
              'rol' => 'usuario',
              'activo' => 1,
              'direccion' => 'Rep. de Argentina, Saltillo, Coahuila',
              'created_at' => new DateTime,
              'updated_at' => new DateTime
              ]
        );
        DB::table('users')->insert($data);
    }
}
