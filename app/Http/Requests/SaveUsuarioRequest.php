<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      return [
          'nombres'      => 'required|max:100',
          'apellidos' => 'required|max:100',
          'correo'     => 'required|email|unique:users',
          'usuario'      => 'required|unique:users|min:4|max:20',
          'password'  => 'required|confirmed',
          'rol'      => 'required|in:usuario,administrador'
      ];
    }
}
