<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class StoreController extends Controller
{
    public function index(){
      $productos = Producto::all();

      return view('store.index', compact('productos'));
    }

    public function show($url){
      $producto = Producto::where('url', $url)->first();
      return view('store.show', compact('producto'));
    }
}
