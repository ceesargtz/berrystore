<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
class CartController extends Controller
{
    public function __construct(){
      if(!\Session::has('carrito')) \Session::put('carrito',array());
    }

    //mostrar
    public function show(){
      $carrito = \Session::get('carrito');
      $total= $this->total();
      return view('store.carrito', compact('carrito','total'));
    }

    //agregar
    public function add(Producto $producto){
      $carrito = \Session::get('carrito');
      $producto->cantidad =1;
      $carrito[$producto->url]=$producto;
      \Session::put('carrito', $carrito);
      return redirect()->route('mostrar-carrito');
    }

    //Eliminar
    public function delete(Producto $producto){
      $carrito = \Session::get('carrito');
      unset($carrito[$producto->url]);
      \Session::put('carrito', $carrito);
      return redirect()->route('mostrar-carrito');
    }

    public function trash(){
      \Session::forget('carrito');
      return redirect()->route('mostrar-carrito');
    }

    //Actualizar
    public function update(Producto $producto, $cantidad){
      $carrito = \Session::get('carrito');
      $carrito[$producto->url]->cantidad = $cantidad;
      \Session::put('carrito',$carrito);
      return redirect()->route('mostrar-carrito');
    }

    private function total(){
        $carrito = \Session::get('carrito');
        $total=0;
        foreach ($carrito as $item) {
        $total+=$item->precio * $item->cantidad;
        }
        return $total;
    }

    //Detalle del pedido
    public function orderDetail(){
      if(count(\Session::get('carrito'))<=0) return redirect()->route('store');
      $carrito = \Session::get('carrito');
      $total=$this->total();

      return view('store.detalle-orden', compact('carrito','total'));
    }
}
