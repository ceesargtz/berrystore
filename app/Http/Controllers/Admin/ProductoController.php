<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Producto;
use App\Categoria;
use App\Http\Requests;
use App\Http\Requests\SaveProductoRequest;

class ProductoController extends Controller
{
  public function index(){
    $productos= Producto::orderBy('id','desc')->paginate(5);
    return view('admin.producto.index', compact('productos'));
  }

  public function create(){
    $categorias = Categoria::orderBy('id','desc')->pluck('nombre','id');
    return view('admin.producto.create',compact('categorias'));
  }
   public function store(SaveProductoRequest $request){

       // $file = $request->file($request->get('nombre'));
       // $nombre_imagen = time(). $file->getClientOriginalName();
       // $file->move(public_path().'/imagenes/',$nombre_imagen);

      $producto = Producto::create([
          'nombre' => $request->get('nombre'),
          'url' => str_slug($request->get('nombre')),
          'descripcion' => $request->get('descripcion'),
          'descripcion_corta' => $request->get('descripcion_corta'),
          'precio' => $request->get('precio'),
          'imagen' => $request->get('imagen'),
          'visible' => $request -> has('visible') ? 1 : 0,
          'id_categoria' => $request->get('id_categoria')
      ]);
     $message = $producto ? 'Producto agregado correctamente!' : 'El producto NO pudo agregarse!';
      return redirect()->route('producto.index')->with('message', $message);
 }

  public function show(Producto $producto){
    return $producto;
  }

  public function edit(Producto $producto){
    $categorias = Categoria::orderBy('id','desc')->pluck('nombre','id');
    return view('admin.producto.edit',compact('producto','categorias'));
  }
   public function update(SaveProductoRequest $request, Producto $producto){

     $producto->fill($request->all());
     $producto->url = str_slug($request->get('nombre'));
     $producto->visible = $request->has('visible') ? 1 : 0;

        $updated = $producto->save();

        $message = $updated ? 'Producto actualizado correctamente!' : 'El Producto NO pudo actualizarse!';

        return redirect()->route('producto.index')->with('message', $message);
 }
  //
  public function destroy(Producto $producto){
    // $producto= Producto::find($producto);
    $deleted = $producto->delete();
    $message = $deleted ? 'Producto eliminado correctamente' : 'El producto no se pudo eliminar';
    return redirect()-> route('producto.index')->with ('message', $message);
  }
}
