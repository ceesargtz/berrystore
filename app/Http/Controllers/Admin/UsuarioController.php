<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\SaveUsuarioRequest;

class UsuarioController extends Controller
{
  public function index(){
    $usuarios= User::orderBy('nombres')->paginate(5);
    return view('admin.usuario.index', compact('usuarios'));
  }

  public function create(){
    return view('admin.usuario.create');
  }
   public function store(SaveUsuarioRequest $request){
     $usuario = User::create([
            'nombres'          => $request->get('nombres'),
            'apellidos'     => $request->get('apellidos'),
            'correo'         => $request->get('correo'),
            'usuario'          => $request->get('usuario'),
            'password'      => $request->get('password'),
            'rol'          => $request->get('rol'),
            'activo'        => $request->has('activo') ? 1 : 0,
            'direccion'       => $request->get('direccion')
        ]);
        $message = $usuario ? 'Usuario agregado correctamente!' : 'El usuario NO pudo agregarse!';
        return redirect()->route('usuario.index')->with('message', $message);
 }

  public function show(Producto $usuario){
    return $usuario;
  }

  public function edit(User $usuario){
    return view('admin.usuario.edit',compact('usuario'));
  }
   public function update(Request $request, User $usuario){
     $this->validate($request, [
          'nombres'      => 'required|max:100',
          'apellidos' => 'required|max:100',
          'correo'     => 'required|email|unique:users',
          'usuario'      => 'required|min:4|max:20',
          'password'  => ($request->get('password') != "") ? 'required|confirmed' : "",
          'rol'      => 'required|in:usuario,administrador',
      ]);

      $usuario->nombres = $request->get('nombres');
      $usuario->apellidos = $request->get('apellidos');
      $usuario->correo = $request->get('correo');
      $usuario->usuario = $request->get('usuario');
      $usuario->rol = $request->get('rol');
      $usuario->direccion = $request->get('direccion');
      $usuario->activo = $request->has('activo') ? 1 : 0;
      if($request->get('password') != "") $usuario->password = $request->get('password');

      $updated = $usuario->save();

      $message = $updated ? 'Usuario actualizado correctamente!' : 'El Usuario NO pudo actualizarse!';

      return redirect()->route('usuario.index')->with('message', $message);
 }
  //
  public function destroy(User $usuario){
    $deleted = $usuario->delete();
    $message = $deleted ? 'Usuario eliminado correctamente' : 'El usuario no se pudo eliminar';
    return redirect()-> route('usuario.index')->with ('message', $message);
  }
}
