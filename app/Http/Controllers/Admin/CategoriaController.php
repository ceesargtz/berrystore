<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Categoria;
use App\Http\Requests;

class CategoriaController extends Controller
{
    public function index(){
      $categorias= Categoria::all();
      return view('admin.categoria.index', compact('categorias'));
    }

    public function create(){
      return view('admin.categoria.create');
    }

    public function store(Request $request){

        $this->validate($request, [
          'nombre' => 'required|unique:categorias|max:255'
        ]);

        $categoria = Categoria::create([
            'nombre' => $request->get('nombre'),
            'url' => str_slug($request->get('nombre')),
            'descripcion' => $request->get('descripcion')
        ]);

        $message = $categoria ? 'Categoría agregada correctamente!' : 'La Categoría NO pudo agregarse!';

        return redirect()->route('categoria.index')->with('message', $message);
    }

    public function show(Categoria $categoria){
      return $categoria;
    }

    public function edit($categoria){
      $categoria = Categoria::find($categoria);
      return view('admin.categoria.edit',compact('categoria'));
    }

    public function update(Request $request,$categoria){
        $categoria= Categoria::find($categoria);
        // $categoria->fill($request->all());
        $this->validate($request, [
          'nombre' => 'required|unique:categorias|max:255'
        ]);
        $categoria->url = str_slug($request->get('nombre'));
        $updated = $categoria->save();
       $message = $updated ? 'Categoria actualizada correctamente!' : 'La categoria no pudo actualizarse!';
      return redirect()-> route('categoria.index')->with ('message', $message);
    }

    public function destroy($categoria){
      $categoria= Categoria::find($categoria);
      $deleted = $categoria->delete();
      $message = $deleted ? 'Categoría eliminada correctamente' : 'La categoria no se pudo eliminar';
      return redirect()-> route('categoria.index')->with ('message', $message);
    }
}
