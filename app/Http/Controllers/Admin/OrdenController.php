<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Orden;
use App\OrdenItem;
use App\Producto;

class OrdenController extends Controller
{
    public function index(){
      $ordenes = Orden::orderBy('created_at','desc')->paginate(5);
      return view('admin.orden.index', compact('ordenes'));
    }

    public function getItems(Request $request){
      $items = OrdenItem::with('producto')->where('id_orden', $request->get('id_orden'))->get();
    return json_encode($items);
    }

    public function destroy($id){
      $orden = Orden::findOrFail($id);
      $deleted= $orden->delete();
      $message = $deleted ? 'Pedido eliminado correctamente' : 'El pedido no se pudo eliminar';
      return redirect()-> route('admin.orden.index')->with ('message', $message);
    }
}
