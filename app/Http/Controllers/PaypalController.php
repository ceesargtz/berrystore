<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Routing\Controller;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Orden;
use App\OrdenItem;


class PaypalController extends BaseController
{
  private $_api_context;
public function __construct()
{
  // setup PayPal api context
  $paypal_conf = \Config::get('paypal');
  $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
  $this->_api_context->setConfig($paypal_conf['settings']);
}
public function postPayment()
{
  $payer = new Payer();
  $payer->setPaymentMethod('paypal');
  $items = array();
  $subtotal = 0;
  $cart = \Session::get('carrito');
  $currency = 'MXN';

  //checar cada item del carrito e ir sumando precios
  foreach($cart as $producto){
    $item = new Item();
    $item->setName($producto->nombre)
    ->setCurrency($currency)
    ->setDescription($producto->descripcion_corta)
    ->setQuantity($producto->cantidad)
    ->setPrice($producto->precio);
    $items[] = $item;
    $subtotal += $producto->cantidad * $producto->precio;
  }
  //guardar en lista de items
  $item_list = new ItemList();
  $item_list->setItems($items);
  //costo de envio
  $details = new Details();
  $details->setSubtotal($subtotal)
  ->setShipping(100);

  $total = $subtotal + 100;
  $amount = new Amount();
  $amount->setCurrency($currency)
    ->setTotal($total)
    ->setDetails($details);

  $transaction = new Transaction();
  $transaction->setAmount($amount)
    ->setItemList($item_list)
    ->setDescription('Pedido de prueba en mi Berry Store');

  //redirección en caso de exito o de cancelacion de pago
  $redirect_urls = new RedirectUrls();
  $redirect_urls->setReturnUrl(\URL::route('payment.status'))
    ->setCancelUrl(\URL::route('payment.status'));

  //Sale = venta directa
  $payment = new Payment();
  $payment->setIntent('Sale')
    ->setPayer($payer)
    ->setRedirectUrls($redirect_urls)
    ->setTransactions(array($transaction));

  //se manda y verifica la transaccion, en caso de error nos lo mostrara
  try {
    $payment->create($this->_api_context);
  } catch (\PayPal\Exception\PPConnectionException $ex) {
    if (\Config::get('app.debug')) {
      echo "Exception: " . $ex->getMessage() . PHP_EOL;
      $err_data = json_decode($ex->getData(), true);
      exit;
    } else {
      die('Algo salió mal');
    }
  }

  foreach($payment->getLinks() as $link) {
    if($link->getRel() == 'approval_url') {
      $redirect_url = $link->getHref();
      break;
    }
  }
  // add payment ID to session
  \Session::put('paypal_payment_id', $payment->getId());
  if(isset($redirect_url)) {
    // redirect to paypal
    return \Redirect::away($redirect_url);
  }
  return \Redirect::route('mostrar-carrito')
    ->with('error', 'Ups! Error desconocido.');
}

public function getPaymentStatus()
{
  // Get the payment ID before session clear
  $payment_id = \Session::get('paypal_payment_id');

  // clear the session payment ID
  \Session::forget('paypal_payment_id');
  $payerId = Request::get('PayerID');
  $token = Request::get('token');

  //if (empty(Request::get('PayerID')) || empty(Request::get('token'))) {
  if (empty($payerId) || empty($token)) {
    return \Redirect::route('store')
      ->with('message', 'Hubo un problema al intentar pagar con Paypal');
  }

  //si todo sale bien
  $payment = Payment::get($payment_id, $this->_api_context);
  // PaymentExecution object includes information necessary
  // to execute a PayPal account payment.
  // The payer_id is added to the request query parameters
  // when the user is redirected from paypal back to your site
  $execution = new PaymentExecution();
  $execution->setPayerId(Request::get('PayerID'));
  //Execute the payment
  $result = $payment->execute($execution, $this->_api_context);
  //echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later
  if ($result->getState() == 'approved') { // payment made
    // Registrar el pedido --- ok
    // Registrar el Detalle del pedido  --- ok
    // Eliminar carrito
    // Enviar correo a user
    // Enviar correo a admin
    // Redireccionar
    //Registrar el pedido y eliminar la sesion del carrito
    $this->saveOrder(\Session::get('carrito'));
    \Session::forget('carrito');
    return \Redirect::route('store')
      ->with('message', 'Compra realizada de forma correcta');
  }
  return \Redirect::route('store')
    ->with('message', 'La compra fue cancelada');
}
private function saveOrder($carrito)
{
    $subtotal = 0;
    foreach($carrito as $item){
        $subtotal += $item->precio * $item->cantidad;
    }

    $order = Orden::create([
        'subtotal' => $subtotal,
        'envio' => 100,
        'id_usuario' => \Auth::user()->id
    ]);

    foreach($carrito as $item){
        $this->saveOrderItem($item, $order->id);
    }
}

private function saveOrderItem($item, $order_id)
{
  OrdenItem::create([
    'cantidad' => $item->cantidad,
    'precio' => $item->precio,
    'id_producto' => $item->id,
    'id_orden' => $order_id
  ]);
}
}
