<?php

namespace App;
use App\Producto;
use App\Orden;

use Illuminate\Database\Eloquent\Model;

class OrdenItem extends Model
{
  protected $table = 'orden_items';
  protected $fillable = ['precio','cantidad','id_producto','id_orden'];
  public $timestamps=false;

  public function orden(){
    return $this->belongsTo('App\Orden');
  }
  public function producto(){
    return $this->belongsTo('App\Producto');
  }
}
