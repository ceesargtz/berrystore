<?php

namespace App;
use App\User;
use App\OrdenItem;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    protected $table = 'ordenes';
    protected $fillable = ['subtotal','envio','id_usuario'];

    public function usuario(){
      return $this->belongsTo('App\User','id_usuario')->first();
    }

    public function orden_items(){
      return $this->hasMany('App\OrdenItem');
    }
}
