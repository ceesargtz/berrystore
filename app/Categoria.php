<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Producto;

class Categoria extends Model
{
  protected $table = 'categorias';
  protected $fillable = ['nombre','url','descripcion'];
  public $timestamps = false;

  public function productos(){
    return $this->hasMany('App\Producto');
  }
}
