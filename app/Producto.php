<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria;

class Producto extends Model
{
  protected $table = 'productos';
  protected $fillable = ['nombre','url','descripcion','descripcion_corta','precio','imagen','visible','id_categoria'];
  public $timestamps = true;

  public function categoria(){
    return $this->belongsTo('App\Categoria','id_categoria')->first();
  }
  public function orden_item(){
    return $this->hasOne('App\OrdenItem');
  }
}
