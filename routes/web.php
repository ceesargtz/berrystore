<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//injeccion de dependencia
Route::bind('producto',function($url){
  return App\Producto::where('url',$url)->first();
});

Route::bind('categoria',function($categoria){
  return App\Categoria::find($categoria);
});

Route::bind('usuario',function($usuario){
  return App\User::find($usuario);
});

Route::bind('orden',function($orden){
  return App\Orden::find($orden);
});

Route::get('/', [
  'as'=>'store',
  'uses'=>'StoreController@index'
  ]);

  Route::get('producto/{url}', [
    'as'=>'producto-detalle',
    'uses'=>'StoreController@show'
    ]);
//Carrito
 Route::get('carrito/mostrar',[
  'as' => 'mostrar-carrito',
  'uses'=>'CartController@show'
  ]);

  Route::get('carrito/agregar/{producto}',[
  'as'=> 'agregar-carrito',
  'uses' => 'CartController@add'
]);

  Route::get('carrito/eliminar/{producto}',[
  'as'=> 'eliminar-carrito',
  'uses' => 'CartController@delete'
  ]);

  Route::get('carrito/trash',[
  'as'=> 'carrito-trash',
  'uses' => 'CartController@trash'
  ]);

  Route::get('carrito/actualizar/{producto}/{cantidad?}',[
  'as'=> 'actualizar-carrito',
  'uses' => 'CartController@update'
  ]);

  Route::get('detalle-orden',[
    'middleware'=>'auth',
    'as' => 'detalle-orden',
    'uses'=> 'CartController@orderDetail'
  ]);

//Authentication
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@authenticate');
Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
})->name('logout');

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Route::get('/home', 'HomeController@index')->name('home');

//paypal
//Enviamos nuestro pedido
Route::get('payment',array(
  'as'=>'payment',
  'uses'=>'PaypalController@postPayment'
));
//Paypa redirecciona a esta ruta
Route::get('payment/status',array(
  'as'=>'payment.status',
  'uses'=>'PaypalController@getPaymentStatus'
));

//admin
Route::group(['namespace' => 'Admin', 'middleware' => ['admin'], 'prefix' => 'admin'], function()
{

  Route::get('home', function(){
    return view('admin.home');
  });

  Route::resource('categoria','CategoriaController');
  Route::resource('producto','ProductoController');
  Route::resource('usuario','UsuarioController');

  Route::get('ordenes',[
    'as' => 'admin.orden.index',
    'uses' => 'OrdenController@index'
  ]);

  Route::post('orden/get-items',[
    'as' => 'admin.orden.getItems',
    'uses' => 'OrdenController@getItems'
  ]);

  Route::post('orden/{id}',[
    'as' => 'admin.orden.destroy',
    'uses' => 'OrdenController@destroy'
  ]);
});
