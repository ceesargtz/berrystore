<footer class="bg-dark">
  <div class="container-fluid row">
    <div class="col-md-6">
      <h4>Dudas e inquietudes contactanos en:</h4> <br>
      <h5><i class="fa fa-phone-square"></i>  (844) 608-29-11</h5>
      <h5><i class="fa fa-envelope" aria-hidden="true"></i>  ceesar_gtz@hotmail.com</h5>
    </div>
    <div class="col-md-6 footer-redes">
      <h4>Siguenos en:</h4> <br>
      <ul class="footer-logos">
        <li>
          <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
        </li>
        <li>
          <a href="#"><i class="fa fa-twitter-square fa-3x"></i></a>
        </li>
        <li>
          <a href="#"><i class="fa fa-linkedin-square fa-3x"></i></a>
        </li>
      </ul>
    </div>
  </div>
</footer>
