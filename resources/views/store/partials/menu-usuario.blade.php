@if(Auth::check())
<!-- Example single danger button -->

<div class="btn-group">
  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{Auth::user()->usuario}}
  </button>
  <div class="dropdown-menu dropdown-menu-right">
    @if(Auth::user()->rol =='administrador')
      <a class="dropdown-item" href="{{url('admin/home')}}"  name="button">
        Dashboard
      </a>
    @endif
    <a class="dropdown-item" href="{{route('logout')}}">Cerrar Sesión</a>
  </div>
</div>
@else
  <a class="btn btn-outline-danger  mb-3 mb-md-0 ml-md-3" href="{{route('login')}}"><i class="fa fa-user fa-lg mr-3"></i>Ingresar</a>
@endif
