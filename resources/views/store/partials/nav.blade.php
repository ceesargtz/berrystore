<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a href="{{route('store')}}">
    <img src="{{ asset('imagenes/logo.png') }}"  width="180" height="45" class="d-inline-block align-top">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="{{route('mostrar-carrito')}}"> <i class="fa fa-shopping-cart fa-2x"></i> </a>
      </li>
    </ul>
    @include('store.partials.menu-usuario')
  </div>

</nav>
