@extends('store.template')
@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1> <i class="fa fa-shopping-cart"></i> Carrito de compras </h1>
    </div>
    <div class="table-carrito">
      @if(count($carrito))

      <p>
        <a href="{{route('carrito-trash')}}" class="btn btn-danger">
          Vaciar carrito <i class="fa fa-trash"></i>
        </a>
      </p>

      <div class="table-responsive">
        <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Imagen</th>
            <th scope="col">Producto</th>
            <th scope="col">Precio</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Subtotal</th>
            <th scope="col">Quitar</th>
          </tr>
        </thead>
        <tbody class="bg-light">
          @foreach($carrito as $item)
          <tr>
            <td> <img src="{{$item->imagen}}" alt=""> </td>
            <td>{{$item->nombre}}</td>
            <td>${{number_format($item->precio,2)}}</td>
            <td>
              <input type="number" min="1" max="100" value="{{$item->cantidad}}" id="producto_{{$item->id}}">
              <a  class="btn btn-warning btn-update-item"
                data-href="{{route('actualizar-carrito', $item->url)}}"
                data-id="{{$item->id}}">
                <i class="fa fa-refresh"></i>
              </a>
            </td>
            <td>${{number_format($item->precio * $item->cantidad,2)}}</td>
            <td>
              <a href="{{route('eliminar-carrito',$item->url)}}" class="btn btn-danger">
                <i class="fa fa-remove"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table><hr>
      <h3> Total: ${{number_format($total,2)}}  </h3>
      </div>
      @else
      <div class="alert alert-danger" role="alert">
          No hay articulos en el carrito
      </div>
      @endif
      <hr>
      <p>
        <a href="{{route('store')}}" class="btn btn-primary">
          <i class="fa fa-chevron-circle-left"></i> Seguir comprando
        </a>
        <a href="{{route('detalle-orden')}}" class="btn btn-primary">
          Continuar <i class="fa fa-chevron-circle-right"></i>
        </a>
      </p>
    </div>
  </div>
@stop
