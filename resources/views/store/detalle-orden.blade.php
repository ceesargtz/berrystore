@extends('store.template')
@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1> <i class="fa fa-shooping-cart"></i> Detalle del pedido</h1>
    </div>
  </div>

  <div class="page container text-center">
    <div class="table-responsive">
      <h3>Datos del usuario</h3>
      <table class="table table-striped table-hover table-bordered">
        <tr><td>Nombre: </td> <td>{{Auth::user()->nombres ." ". Auth::user()->apellidos}}</td> </tr>
        <tr><td>Usuario: </td> <td>{{Auth::user()->usuario}}</td> </tr>
        <tr><td>Correo: </td> <td>{{Auth::user()->correo}}</td> </tr>
        <tr><td>Dirección: </td> <td>{{Auth::user()->direccion}}</td> </tr>
      </table>
    </div>
    <div class="table-responsive">
      <h3>Datos del pedido</h3>
      <table class="table table-striped table-hover table-bordered">
        <thead class="thead-dark">
          <tr>
            <th>Producto</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        @foreach($carrito as $item)
        <tr>
          <td>{{$item->nombre}}</td>
          <td>${{number_format($item->precio,2)}}</td>
          <td>{{$item->cantidad}}</td>
          <td>${{number_format($item->precio * $item->cantidad,2)}}</td>
        </tr>
        @endforeach
      </table><hr>
          <h3> Total: ${{number_format($total,2)}}  </h3><hr>
          <p>
            <a href="{{route('mostrar-carrito')}}" class="btn btn-primary">
              <i class="fa fa-chevron-circle-left"></i> Regresar
            </a>
            <a href="{{route('payment')}}" class="btn btn-warning">
              Pagar con <i class="fa fa-cc-paypal fa-2x"></i>
            </a>
          </p>
    </div>
  </div>
@stop
