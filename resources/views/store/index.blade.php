@extends('store.template')

@section('content')

@include('store.partials.slider')
<div class="container-fluid text-center">
  <div class="page-header">
    <h1>Listado de productos</h1>
  </div>
     <div class="productos card-columns">
       @foreach ($productos as $producto)
       <div class="producto card text-center">
         <div class="text-center">
           <img src="{{$producto->imagen}}" class="imagen-producto" width="250">
         </div>
           <div class="producto-info card-body">
             <h3 class="card-title">{{$producto->nombre}}</h3>
             <p>{{$producto->descripcion_corta}}</p>
             <h5><small class="badge badge-precio">Precio: {{number_format($producto->precio,2)}}</small>
             </h5>
             <p>
               <a class="btn btn-primary" href="{{route('agregar-carrito', $producto->url)}}">
                 <i class="fa fa-shopping-cart fa-lg"></i> Añadir a carrito</a>
               <a class="btn btn-danger" href="{{route('producto-detalle', $producto->url)}}">Leer más</a>
             </p>
           </div>
       </div>
       @endforeach
     </div>
</div>

@stop
