@extends('store.template')

@section('content')
<div class="container text-center">
  <div class="page-header">
    <h1><i class="fa fa-shopping-cart fa-lg"></i>  Detalle del Producto</h1>
  </div> <hr>
  <div class="row">
    <div class="col-md-6">
      <div class="producto-bloque">
        <img src="{{$producto->imagen}}" width="400px" >
      </div>
    </div>
    <div class="col-md-6">
      <div class="producto-bloque bg-light">
        <h3>{{$producto->nombre}}</h3><hr>
        <div class="producto-info panel">
          <p>{{$producto->descripcion}}</p>
          <h5><small class="badge badge-precio">Precio: {{number_format($producto->precio,2)}}</small></h5>
          <p>
            <a class="btn btn-primary btn-block" href="{{route('agregar-carrito', $producto->url)}}">
              <i class="fa fa-cart-plus fa-lg"></i> Añadir a carrito</a>
          </p>
        </div>
      </div>
    </div>
  </div> <hr>
  <p> <a class="btn btn-danger"href="{{route('store')}}">
      <i class="fa fa-chevron-circle-left"></i> Regresar
  </a> </p>
</div>

@stop
