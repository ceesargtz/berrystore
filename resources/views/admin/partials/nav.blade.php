<nav class="navbar navbar-expand-lg navbar-default navbar-light bg-light">
  <a href="{{'home'}}">
    <img src="{{ asset('imagenes/logo.png') }}"  width="180" height="45" class="d-inline-block align-top">
  </a>
  <div class="dashboard ml-5">
    <h3> <i class="fa fa-dashboard"></i> Dashboard</h3>
  </div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="nav navbar-nav ml-auto">
      <li ><a href="{{route('categoria.index')}}"><h5>Categorias</h5></a> </li>
      <li ><a href="{{route('producto.index')}}"><h5>Productos</h5></a></li>
      <li ><a href="{{'ordenes'}}"><h5>Pedidos</h5></a></li>
      <li ><a href="{{route('usuario.index')}}"><h5>Usuarios</h5></a></li>

    <div class="btn-group">
      <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{Auth::user()->usuario}}
      </button>
      <div class="dropdown-menu dropdown-menu-right">
        @if(Auth::user()->rol =='administrador')
          <a class="dropdown-item" href="{{url('')}}"  name="button">
            Ver la página
          </a>
        @endif
        <a class="dropdown-item" href="{{route('logout')}}">Cerrar Sesión</a>
      </div>
    </div>
    </ul>
  </div>

</nav>
