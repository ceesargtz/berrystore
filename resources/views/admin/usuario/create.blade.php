@extends('admin.template')

@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1>
        <i class="fa-fa-shopping-cart"></i> Usuarios
        <small>[Agregar usuario]</small>
      </h1>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-offset-3 col-md-6">
                <div class="page bg-light text-center">
                    @if (count($errors) > 0)
                        @include('admin.partials.errores')
                    @endif
                    {!! Form::open(['route'=>'usuario.store']) !!}

                    <div class="form-group">
                        <label for="nombres">Nombres:</label>

                        {!!
                            Form::text(
                                'nombres',
                                null,
                                array(
                                    'class'=>'form-control',
                                    'placeholder' => 'Ingresa el nombre...',
                                    'autofocus' => 'autofocus',
                                    //'required' => 'required'
                                )
                            )
                        !!}
                    </div>

                    <div class="form-group">
                        <label for="apellidos">Apellidos:</label>

                        {!!
                            Form::text(
                                'apellidos',
                                null,
                                array(
                                    'class'=>'form-control',
                                    'placeholder' => 'Ingresa los apellidos...',
                                    //'required' => 'required'
                                )
                            )
                        !!}
                    </div>

                    <div class="form-group">
                        <label for="correo">Correo:</label>

                        {!!
                            Form::text(
                                'correo',
                                null,
                                array(
                                    'class'=>'form-control',
                                    'placeholder' => 'Ingresa el correo...',
                                    //'required' => 'required'
                                )
                            )
                        !!}
                    </div>

                    <div class="form-group">
                        <label for="usuario">Usuario:</label>

                        {!!
                            Form::text(
                                'usuario',
                                null,
                                array(
                                    'class'=>'form-control',
                                    'placeholder' => 'Ingresa el nombre de usuario...',
                                    //'required' => 'required'
                                )
                            )
                        !!}
                    </div>

                    <div class="form-group">
                        <label for="password">Password:</label>

                        {!!
                            Form::password(
                                'password',
                                array(
                                    'class'=>'form-control',
                                    //'required' => 'required'
                                )
                            )
                        !!}
                    </div>

                    <div class="form-group">
                        <label for="confirm_password">Confirmar Password:</label>

                        {!!
                            Form::password(
                                'password_confirmation',
                                array(
                                    'class'=>'form-control',
                                    //'required' => 'required'
                                )
                            )
                        !!}
                    </div>

                    <div class="form-group">
                        <label for="rol">Tipo:</label>

                        {!! Form::radio('rol', 'usuario', true) !!} Usuario
                        {!! Form::radio('rol', 'administrador') !!} Administrador
                    </div>

                    <div class="form-group">
                        <label for="direccion">Dirección:</label>

                        {!!
                            Form::textarea(
                                'direccion',
                                null,
                                array(
                                    'class'=>'form-control'
                                )
                            )
                        !!}
                    </div>

                    <div class="form-group">
                        <label for="activo">Activo:</label>

                        {!! Form::checkbox('activo', null, true) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Guardar', array('class'=>'btn btn-primary')) !!}
                        <a href="{{ route('usuario.index') }}" class="btn btn-warning">Cancelar</a>
                    </div>

                   {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
@stop
