@extends('admin.template')
@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1>
        <i class="fa-fa-shopping-cart"></i> Usuarios
        <a href="{{route('usuario.create')}}" class="btn btn-primary"> <i class="fa fa-plus-circle"></i> Usuario</a>
      </h1>
    </div>
    <div class="page">
      <div class="table-responsive">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Editar</th>
              <th scope="col">Eliminar</th>
              <th scope="col">Nombres</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Nombre de usuario</th>
              <th scope="col">Correo</th>
              <th scope="col">Tipo</th>
              <th scope="col">Activo</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($usuarios as $usuario)
            <tr>
              <td>
                <a href="{{route('usuario.edit', $usuario) }}" class="btn btn-warning">
                  <i class="fa fa-pencil-square"></i>
                </a>
              </td>
              <td>
                {!! Form::open(['route' => ['usuario.destroy', $usuario]]) !!}
        								<input type="hidden" name="_method" value="DELETE">
        								<button onClick="return confirm('Eliminar registro?')" class="btn btn-danger">
        									<i class="fa fa-trash-o"></i>
        								</button>
        							{!! Form::close() !!}
              </td>
              <td>{{$usuario->nombres}}</td>
              <td>{{$usuario->apellidos}}</td>
              <td>{{$usuario->usuario}}</td>
              <td>{{$usuario->correo}}</td>
              <td>{{$usuario->rol}}</td>
              <td>{{$usuario->active == 1 ? "Sí" : "No"}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <hr>
      <?php echo $usuarios->render(); ?>
    </div>
  </div>

@stop
