@extends('admin.template')
@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1>
        <i class="fa-fa-shopping-cart"></i> Productos
        <a href="{{route('producto.create')}}" class="btn btn-primary"> <i class="fa fa-plus-circle"></i> Categoría</a>
      </h1>
    </div>
    <div class="page">
      <div class="table-responsive">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Editar</th>
              <th scope="col">Eliminar</th>
              <th scope="col">Imagen</th>
              <th scope="col">Nombre</th>
              <th scope="col">Categoría</th>
              <th scope="col">Descripción corta</th>
              <th scope="col">Precio</th>
              <th scope="col">Visible</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($productos as $producto)
            <tr>
              <td>
                <a href="{{route('producto.edit', $producto->url) }}" class="btn btn-warning">
                  <i class="fa fa-pencil-square"></i>
                </a>
              </td>
              <td>
                {!! Form::open(['route' => ['producto.destroy', $producto->url]]) !!}
        								<input type="hidden" name="_method" value="DELETE">
        								<button onClick="return confirm('Eliminar registro?')" class="btn btn-danger">
        									<i class="fa fa-trash-o"></i>
        								</button>
        							{!! Form::close() !!}
              </td>
              <td> <img src="{{$producto->imagen}}" width="40"> </td>
              <td>{{$producto->nombre}}</td>
              <td>{{$producto->categoria()->nombre}}</td>
              <td>{{$producto->descripcion_corta}}</td>
              <td>${{number_format($producto->precio,2)}}</td>
              <td>{{$producto->visible == 1 ? "Sí" : "No"}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <hr>
      <?php echo $productos->render(); ?>
    </div>
  </div>

@stop
