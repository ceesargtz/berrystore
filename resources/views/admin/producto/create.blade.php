@extends('admin.template')

@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1>
        <i class="fa-fa-shopping-cart"></i> Productos
        <small>[Agregar producto]</small>
      </h1>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-offset-3 col-md-6">
                <div class="page bg-light text-center">
                    @if (count($errors) > 0)
                        @include('admin.partials.errores')
                    @endif
                    {!! Form::open(['route'=>'producto.store']) !!}

                       <div class="form-group">
                           <label class="control-label" for="category_id">Categoría</label>
                           {!! Form::select('id_categoria', $categorias, null, ['class' => 'form-control']) !!}
                       </div>

                       <div class="form-group">
                           <label for="name">Nombre:</label>

                           {!!
                               Form::text(
                                   'nombre',
                                   null,
                                   array(
                                       'class'=>'form-control',
                                       'placeholder' => 'Ingresa el nombre...',
                                       'autofocus' => 'autofocus'
                                   )
                               )
                           !!}
                       </div>

                       <div class="form-group">
                           <label for="extract">Descripción corta:</label>

                           {!!
                               Form::text(
                                   'descripcion_corta',
                                   null,
                                   array(
                                       'class'=>'form-control',
                                       'placeholder' => 'Ingresa una pequeña descripción...',
                                   )
                               )
                           !!}
                       </div>

                       <div class="form-group">
                           <label for="description">Descripción:</label>

                           {!!
                               Form::textarea(
                                   'descripcion',
                                   null,
                                   array(
                                       'class'=>'form-control'
                                   )
                               )
                           !!}
                       </div>

                       <div class="form-group">
                           <label for="price">Precio:</label>

                           {!!
                               Form::text(
                                   'precio',
                                   null,
                                   array(
                                       'class'=>'form-control',
                                       'placeholder' => 'Ingresa el precio...',
                                   )
                               )
                           !!}
                       </div>

                       <div class="form-group">
                           <label for="image">Imagen:</label>

                           {!!
                               Form::text(
                                   'imagen',
                                   null,
                                   array(
                                       'class'=>'form-control',
                                       'placeholder' => 'Ingresa la url de la imagen...',
                                   )
                               )
                           !!}
                       </div>

                       <div class="form-group">
                           <label for="visible">Visible:</label>

                           {!!
                               Form::checkbox(
                                   'visible',
                                   null,
                                   array(
                                       'class'=>'form-control',
                                   )
                               )
                           !!}
                       </div>

                       <div class="form-group">
                           {!! Form::submit('Guardar', array('class'=>'btn btn-primary')) !!}
                           <a href="{{ route('producto.index') }}" class="btn btn-warning">Cancelar</a>
                       </div>

                   {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
@stop
