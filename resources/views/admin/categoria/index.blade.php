@extends('admin.template')

@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1>
        <i class="fa-fa-shopping-cart"></i> Categorías
        <a href="{{route('categoria.create')}}" class="btn btn-primary"> <i class="fa fa-plus-circle"></i> Categoría</a>
      </h1>
    </div>
    <div class="page">
      <div class="table-responsive">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Editar</th>
              <th scope="col">Eliminar</th>
              <th scope="col">Nombre</th>
              <th scope="col">Descripcion</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($categorias as $categoria)
            <tr>
              <td>
                <a href="{{route('categoria.edit', $categoria) }}" class="btn btn-warning">
                  <i class="fa fa-pencil-square"></i>
                </a>
              </td>
              <td>
                {!! Form::open(['route' => ['categoria.destroy', $categoria]]) !!}
        								<input type="hidden" name="_method" value="DELETE">
        								<button onClick="return confirm('Eliminar registro?')" class="btn btn-danger">
        									<i class="fa fa-trash-o"></i>
        								</button>
        							{!! Form::close() !!}
              </td>
              <td>{{$categoria->nombre}}</td>
              <td>{{$categoria->descripcion}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

@stop
