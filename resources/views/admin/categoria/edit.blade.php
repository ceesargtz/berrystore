@extends('admin.template')

@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1>
        <i class="fa-fa-shopping-cart"></i> Categorías
        <small>[Editar categoría]</small>
      </h1>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-offset-3 col-md-6">
                <div class="page bg-light text-center">
                    @if (count($errors) > 0)
                        @include('admin.partials.errores')
                    @endif

                      <!-- {!! Form::open(['route'=>array('categoria.update',$categoria)])!!} -->
                      {!! Form::model($categoria,['route'=>['categoria.update',$categoria]]) !!}
                    <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="nombre">Nombre:</label>
                            {!!
                                Form::text(
                                    'nombre',
                                    null,
                                    array(
                                        'class'=>'form-control',
                                        'placeholder' => 'Ingresa el nombre...',
                                        'autofocus' => 'autofocus'
                                    )
                                )
                            !!}
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripción:</label>
                            {!!
                                Form::textarea(
                                    'descripcion',
                                    null,
                                    array(
                                        'class'=>'form-control'
                                    )
                                )
                            !!}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::submit('Actualizar', array('class'=>'btn btn-primary')) !!}
                            <a href="{{ route('categoria.index') }}" class="btn btn-warning">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
@stop
