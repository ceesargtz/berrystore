@extends('admin.template')
@section('content')
  <div class="container text-center">
    <div class="page-header">
      <h1>
        <i class="fa-fa-shopping-cart"></i> Pedidos
        <a href="" class="btn btn-primary"> <i class="fa fa-plus-circle"></i> pedidos</a>
      </h1>
    </div>
    <div class="page">
      <div class="table-responsive">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Ver detalle</th>
              <th scope="col">Eliminar</th>
              <th scope="col">Fecha</th>
              <th scope="col">Usuario</th>
              <th scope="col">Subtotal</th>
              <th scope="col">Envío</th>
              <th scope="col">Total</th>
            </tr>
          </thead>
          <tbody>
            @foreach($ordenes as $orden)
              <tr>
                <td>
                    <a href="#"
                       class="btn btn-primary btn-detalle-pedido"
                       data-id="{{ $orden->id }}"
                      data-path="{{ route('admin.orden.getItems') }}"
                      data-toggle="modal"
                      data-target="#myModal"
                      data-token="{{ csrf_token() }}">
                                        <i class="fa fa-external-link"></i>
                                    </a>

                                </td>
                                <td>
                                    {!! Form::open(['route' => ['admin.orden.destroy', $orden->id]]) !!}

        								<button onClick="return confirm('Eliminar registro?')" class="btn btn-danger">
        									<i class="fa fa-trash-o"></i>
        								</button>
        							{!! Form::close() !!}
                                </td>
                                <td>{{ $orden->created_at }}</td>
                                <td>{{ $orden->usuario()->nombres . " " . $orden->usuario()->apellidos }}</td>
                                <td>${{ number_format($orden->subtotal,2) }}</td>
                                <td>${{ number_format($orden->envio,2) }}</td>
                                <td>${{ number_format($orden->subtotal + $orden->envio,2) }}</td>
                            </tr>
                        @endforeach
          </tbody>
        </table>
      </div>
      <hr>
      <?php echo $ordenes->render(); ?>
    </div>
  </div>
  @include('admin.partials.modal-detalle-pedido')
@stop
