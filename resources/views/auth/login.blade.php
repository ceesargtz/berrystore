@extends('store.template')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-3 mb-3 text-center">
                <div class="card-header"><h2>{{ __('Iniciar Sesión') }}</h2></div>

                <div class="card-body">
                  @if ($alert = Session::get('alert-danger'))
	                 <div class="alert alert-danger">
		                   {{ $alert }}
	                    </div>
                      @endif
                    <form method="POST" action="{{route('login')}}">
                        @csrf
                        <div class="form-group row">
                            <label for="correo" class="col-md-4 col-form-label text-md-right"><h5>{{ __('Correo:') }}</h5></label>

                            <div class="col-md-6">
                                <input id="correo" type="correo" class="form-control @error('correo') is-invalid @enderror" name="correo" value="{{ old('correo') }}" required autocomplete="correo" autofocus>

                                @error('correo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right"><h5>{{ __('Contraseña:') }}</h5></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0 ">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('iniciar Sesión') }}
                                </button>
                            </div>
                            <div class="col-md-8 offset-md-2 mt-3">
                            <a  href="{{ route('register') }}">{{ __('¿No tienes una cuenta? Registrate') }}</a>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
